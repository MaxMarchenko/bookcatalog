<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Core\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'FrontController@index')->name('front-page');
Route::get('/authors', 'FrontController@authors')->name('authors-page');
Route::get('/headings', 'FrontController@headings')->name('headings-page');
Route::get('/books/author/{authorName}', 'FrontController@authorBooks')->name('authorBooks');
Route::get('/books/heading/{headingName}', 'FrontController@headingBooks')->name('headingBooks');

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' =>  ['auth', 'admin:' . Permission::VIEW_ADMIN]
], function () {
    Route::get('/', 'IndexController@index')->name('adminIndex');

    Route::post('book/upload-image', 'BookController@uploadImage');
    Route::resource('/book', 'BookController');
    Route::post('/book/authors/id', 'BookController@getAuthorsId');
    Route::post('/book/headings/id', 'BookController@getHeadingsId');

    Route::resource('/author', 'AuthorController');
    Route::resource('/heading', 'HeadingController');

    Route::get('/permissions', 'PermissionsController@index')->name('perms');
    Route::post('/permissions', 'PermissionsController@edit')->name('perms-edit');
});

Route::group([
    'prefix' => 'errors',
    'namespace' => 'Errors'
], function () {
    Route::get('/permission_denied', 'PermissionController@permissionDenied')
        ->name('permission_denied');
});
