<?php

namespace App\Http\Middleware;

use Closure;
use Gate;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $permission
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $permission = 'VIEW_ADMIN')
    {
        if (Gate::denies($permission)) {
            return redirect()->route('permission_denied');
        }

        return $next($request);
    }
}
