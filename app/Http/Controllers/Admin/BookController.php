<?php

namespace App\Http\Controllers\Admin;

use App\Core\Helpers\FileHelper;
use App\Core\Models\Book;
use App\Core\Repositories\BookRepository;
use App\Core\Services\BooksService;
use App\Http\Requests\BookRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class BookController
 * @package App\Http\Controllers\Admin
 */
class BookController extends Controller
{
    /**
     * @var BookRepository $bookRepository
     */
    private $bookRepository;

    /**
     * @var BooksService $bookService
     */
    private $bookService;

    /**
     * BookController constructor.
     * @param BookRepository $bookRepository
     * @param BooksService $bookService
     */
    public function __construct(BookRepository $bookRepository, BooksService $bookService)
    {
        $this->bookRepository = $bookRepository;
        $this->bookService = $bookService;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('admin.book.index', [
            'books' => $this->bookService->getAll(),
            'authors' => $this->bookService->getAuthorNamesArray(),
            'headings' => $this->bookService->getHeadingNamesArray()
        ]);
    }

    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadImage(Request $request): ?string
    {
        $image = $request->file('image');
        return FileHelper::getInstance()->upload($image, FileHelper::FILE_FOLDER_IMAGES);
    }

    /**
     * @param BookRequest $request
     * @return JsonResponse
     */
    public function store(BookRequest $request): JsonResponse
    {
        $book = $this->bookRepository->save($request->all());
        $this->bookRepository->addAuthors($request->get('authors'), $book);
        $this->bookRepository->addHeadings($request->get('headings'), $book);

        return response()->json($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection|null
     */
    public function getAuthorsId(Request $request): ?\Illuminate\Support\Collection
    {
        return $this->bookService->getAuthorsId($request->get('id'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection|null
     */
    public function getHeadingsId(Request $request): ?\Illuminate\Support\Collection
    {
        return $this->bookService->getHeadingsId($request->get('id'));
    }

    /**
     * @param BookRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(BookRequest $request, int $id): ?JsonResponse
    {
        $book = $this->bookRepository->getOne($id);
        if ($book !== null) {
            /** @var Book $book **/
            FileHelper::getInstance()->drop(basename($book->getPhotoName()), FileHelper::DELETE_IMAGE_PATH);
            $book->update($request->all());
            $this->bookRepository->addAuthors($request->get('authors'), $book);
            $this->bookRepository->addHeadings($request->get('headings'), $book);

            return response()->json($request);
        }

        return null;
    }

    /**
     * @param int $id
     * @return JsonResponse}null
     */
    public function destroy(int $id): ?JsonResponse
    {
        $book = $this->bookRepository->getOne($id);
        if ($book !== null) {
            /** @var Book $book **/
            FileHelper::getInstance()->drop(basename($book->getPhotoName()), FileHelper::DELETE_IMAGE_PATH);

            return response()->json($book::destroy($id));
        }

        return null;
    }
}
