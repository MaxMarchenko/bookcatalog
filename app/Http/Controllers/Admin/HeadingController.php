<?php

namespace App\Http\Controllers\Admin;

use App\Core\Repositories\HeadingRepository;
use App\Core\Services\HeadingService;
use App\Http\Requests\HeadingRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class HeadingController
 * @package App\Http\Controllers\Admin
 */
class HeadingController extends Controller
{
    /**
     * @var HeadingRepository $headingRepository
     */
    private $headingRepository;

    /**
     * @var HeadingService $headingService
     */
    private $headingService;

    /**
     * HeadingController constructor.
     * @param HeadingRepository $headingRepository
     * @param HeadingService $headingService
     */
    public function __construct(HeadingRepository $headingRepository, HeadingService $headingService)
    {
        $this->headingRepository = $headingRepository;
        $this->headingService = $headingService;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('admin.heading.index', [
            'headings' => $this->headingService->getAll()
        ]);
    }

    /**
     * @param HeadingRequest $request
     * @return JsonResponse
     */
    public function store(HeadingRequest $request): JsonResponse
    {
        $this->headingRepository->save($request->all());

        return response()->json($request);
    }

    /**
     * @param HeadingRequest $request
     * @param int $id
     * @return JsonResponse|null
     */
    public function update(HeadingRequest $request, int $id): ?JsonResponse
    {
        $heading = $this->headingRepository->getOne($id);
        if ($heading !== null) {
            $heading->update($request->all());

            return response()->json($request);
        }

        return null;
    }


    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return response()->json($this->headingRepository->delete($id));
    }
}
