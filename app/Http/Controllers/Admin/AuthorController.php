<?php

namespace App\Http\Controllers\Admin;

use App\Core\Repositories\AuthorRepository;
use App\Core\Services\AuthorService;
use App\Http\Requests\AuthorRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class AuthorController
 * @package App\Http\Controllers\Admin
 */
class AuthorController extends Controller
{
    /**
     * @var AuthorService $authorService
     */
    private $authorService;

    /**
     * @var AuthorRepository $authorRepository
     */
    private $authorRepository;

    /**
     * AuthorController constructor.
     * @param AuthorService $authorService
     * @param AuthorRepository $authorRepository
     */
    public function __construct(AuthorService $authorService, AuthorRepository $authorRepository)
    {
        $this->authorService = $authorService;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('admin.author.index', [
            'authors' => $this->authorService->getAll()
        ]);
    }

    /**
     * @param AuthorRequest $request
     * @return JsonResponse
     */
    public function store(AuthorRequest $request): JsonResponse
    {
        $this->authorRepository->save($request->all());

        return response()->json($request);
    }

    /**
     * @param AuthorRequest $request
     * @param int $id
     * @return JsonResponse|null
     */
    public function update(AuthorRequest $request, int $id): ?JsonResponse
    {
        $author = $this->authorRepository->getOne($id);
        if ($author !== null) {
            $author->update($request->all());

            return response()->json($request);
        }

        return null;
    }


    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return response()->json($this->authorRepository->delete($id));
    }
}
