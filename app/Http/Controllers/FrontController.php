<?php

namespace App\Http\Controllers;

use App\Core\Repositories\AuthorRepository;
use App\Core\Repositories\HeadingRepository;
use App\Core\Services\AuthorService;
use App\Core\Services\BooksService;
use App\Core\Services\HeadingService;
use Illuminate\View\View;

/**
 * Class FrontController
 * @package App\Http\Controllers\Front
 */
class FrontController extends Controller
{
    /**
     * @var BooksService $bookService
     */
    private $bookService;

    /**
     * @var AuthorService $authorService
     */
    private $authorService;

    /**
     * @var HeadingService $headingService
     */
    private $headingService;

    /**
     * FrontController constructor.
     * @param BooksService $bookService
     * @param AuthorService $authorService
     * @param HeadingService $headingService
     */
    public function __construct(
        BooksService $bookService,
        AuthorService $authorService,
        HeadingService $headingService
    ) {
        $this->bookService = $bookService;
        $this->authorService  = $authorService;
        $this->headingService = $headingService;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('front.index', [
            'books' => $this->bookService->getAll()
        ]);
    }

    /**
     * @param string $authorName
     * @return View
     */
    public function authorBooks(string $authorName): View
    {
        $author = $this->authorService->getOneWithClause([AuthorRepository::NAME_COLUMN => $authorName]);

        if ($author !== null) {
            return view('front.author-books', [
                'author' => $author,
                'authors' => $this->authorService->getAll()
            ]);
        }

        return abort(404);
    }

    /**
     * @param string $headingName
     * @return View
     */
    public function headingBooks(string $headingName): View
    {
        $heading = $this->headingService->getOneWithClause([HeadingRepository::NAME_COLUMN => $headingName]);

        if ($heading !== null) {
            return view('front.heading-books', [
                'heading' => $heading,
                'headings' => $this->headingService->getAll()
            ]);
        }

        return abort(404);
    }

    /**
     * @return View
     */
    public function authors(): View
    {
        return view('front.authors', [
            'authors' => $this->authorService->getAll()
        ]);
    }

    /**
     * @return View
     */
    public function headings(): View
    {
        return view('front.headings', [
            'headings' => $this->headingService->getAll()
        ]);
    }
}
