<?php

namespace App\Http\Controllers\Errors;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class PermissionController
 * @package App\Http\Controllers\Errors
 */
class PermissionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function permissionDenied(): View
    {
        return view('errors.permission_denied');
    }
}
