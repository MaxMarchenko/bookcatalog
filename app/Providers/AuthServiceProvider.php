<?php

namespace App\Providers;

use App\Core\Models\Permission;
use App\Core\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];


    /**
     * Register any authentication / authorization services.
     *
     * @return bool
     */
    public function boot(): bool
    {
        $this->registerPolicies();

        Gate::define(Permission::VIEW_ADMIN, function (User $user) {
            return $user->checkPermission(Permission::VIEW_ADMIN);
        });

        return false;
    }
}
