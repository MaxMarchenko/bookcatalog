<?php

namespace App\Core\DataMappers;

use App\Core\Entities\EntityInterface;
use App\Core\Factories\AuthorEntityFactory;
use App\Core\Factories\BookEntityFactory;
use App\Core\Factories\HeadingEntityFactory;
use App\Core\Models\Author;
use App\Core\Models\Book;
use App\Core\Models\Heading;

/**
 * Class BasicDataMapper
 * @package App\Core\DataMappers
 */
class BasicDataMapper
{
    /**
     * @var BookEntityFactory $bookEntityFactory
     */
    private $bookEntityFactory;

    /**
     * @var HeadingEntityFactory $headingEntityFactory
     */
    private $headingEntityFactory;

    /**
     * @var AuthorEntityFactory $authorEntityFactory
     */
    private $authorEntityFactory;

    /**
     * AbstractDataMapper constructor.
     * @param BookEntityFactory $bookEntityFactory
     * @param HeadingEntityFactory $headingEntityFactory
     * @param AuthorEntityFactory $authorEntityFactory
     */
    public function __construct(
        BookEntityFactory $bookEntityFactory,
        HeadingEntityFactory $headingEntityFactory,
        AuthorEntityFactory $authorEntityFactory
    ) {
        $this->bookEntityFactory = $bookEntityFactory;
        $this->headingEntityFactory = $headingEntityFactory;
        $this->authorEntityFactory = $authorEntityFactory;
    }

    /**
     * @param Book $data
     * @param array $authors
     * @param array $headings
     * @return EntityInterface
     */
    public function setBooksData(Book $data, array $authors, array $headings): EntityInterface
    {
        return $this->bookEntityFactory->create()->setId($data->getId())
            ->setName($data->getName())
            ->setPhotoPath($data->getPhotoName())
            ->setAuthors($authors)
            ->setHeadings($headings);
    }

    /**
     * @param Author $data
     * @return EntityInterface
     */
    public function setAuthorsData(Author $data): EntityInterface
    {
        return $this->authorEntityFactory->create()->setId($data->getId())
            ->setName($data->getName())
            ->setSurname($data->getSurname());
    }

    /**
     * @param Heading $data
     * @return EntityInterface
     */
    public function setHeadingsData(Heading $data): EntityInterface
    {
        return $this->headingEntityFactory->create()->setId($data->getId())
            ->setName($data->getName());
    }
}
