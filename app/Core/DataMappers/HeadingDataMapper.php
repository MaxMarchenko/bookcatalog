<?php

namespace App\Core\DataMappers;

use App\Core\Entities\HeadingEntity;
use App\Core\Factories\AuthorEntityFactory;
use App\Core\Factories\BookEntityFactory;
use App\Core\Factories\HeadingEntityFactory;
use App\Core\Models\Heading;

/**
 * Class HeadingDataMapper
 * @package App\Core\DataMappers
 */
class HeadingDataMapper extends BasicDataMapper
{
    /**
     * @var HeadingEntityFactory $headingEntityFactory
     */
    private $headingEntityFactory;

    /**
     * BookDataMapper constructor.
     * @param BookEntityFactory $bookEntityFactory
     * @param HeadingEntityFactory $headingEntityFactory
     * @param AuthorEntityFactory $authorEntityFactory
     */
    public function __construct(
        BookEntityFactory $bookEntityFactory,
        HeadingEntityFactory $headingEntityFactory,
        AuthorEntityFactory $authorEntityFactory
    ) {
        parent::__construct($bookEntityFactory, $headingEntityFactory, $authorEntityFactory);
        $this->headingEntityFactory = $headingEntityFactory;
    }

    /**
     * @param Heading $data
     * @param array $books
     * @return HeadingEntity
     */
    public function setData(Heading $data, array $books): HeadingEntity
    {
        return $this->headingEntityFactory->create()->setId($data->getId())
            ->setName($data->getName())
            ->setBooks($books);
    }

    /**
     * @param HeadingEntity $entity
     * @return array
     */
    public function toArray(HeadingEntity $entity): array
    {
        return [
            'id'        => $entity->getId(),
            'name'      => $entity->getName(),
            'books'     => $entity->getBooks()
        ];
    }
}
