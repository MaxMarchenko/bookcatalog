<?php

namespace App\Core\DataMappers;

use App\Core\Entities\BookEntity;
use App\Core\Factories\AuthorEntityFactory;
use App\Core\Factories\BookEntityFactory;
use App\Core\Factories\HeadingEntityFactory;
use App\Core\Models\Book;

/**
 * Class BookDataMapper
 * @package App\Core\DataMappers
 */
class BookDataMapper extends BasicDataMapper
{
    /**
     * @var BookEntityFactory $bookEntityFactory
     */
    private $bookEntityFactory;

    /**
     * BookDataMapper constructor.
     * @param BookEntityFactory $bookEntityFactory
     * @param HeadingEntityFactory $headingEntityFactory
     * @param AuthorEntityFactory $authorEntityFactory
     */
    public function __construct(
        BookEntityFactory $bookEntityFactory,
        HeadingEntityFactory $headingEntityFactory,
        AuthorEntityFactory $authorEntityFactory
    ) {
        parent::__construct($bookEntityFactory, $headingEntityFactory, $authorEntityFactory);
        $this->bookEntityFactory = $bookEntityFactory;
    }

    /**
     * @param Book $data
     * @param array $authors
     * @param array $headings
     * @return BookEntity
     */
    public function setData(Book $data, array $authors, array $headings): BookEntity
    {
        return $this->bookEntityFactory->create()->setId($data->getId())
            ->setName($data->getName())
            ->setPhotoPath($data->getPhotoName())
            ->setAuthors($authors)
            ->setHeadings($headings);
    }

    /**
     * @param BookEntity $entity
     * @return array
     */
    public function toArray(BookEntity $entity): array
    {
        return [
            'id'        => $entity->getId(),
            'name'      => $entity->getName(),
            'photo'     => $entity->getPhotoPath(),
            'authors'   => $entity->getAuthors(),
            'headings'  => $entity->getHeadings(),
        ];
    }
}
