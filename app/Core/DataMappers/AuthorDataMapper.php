<?php

namespace App\Core\DataMappers;

use App\Core\Entities\AuthorEntity;
use App\Core\Factories\AuthorEntityFactory;
use App\Core\Factories\BookEntityFactory;
use App\Core\Factories\HeadingEntityFactory;
use App\Core\Models\Author;

/**
 * Class AuthorDataMapper
 * @package App\Core\DataMappers
 */
class AuthorDataMapper extends BasicDataMapper
{
    /**
     * @var AuthorEntityFactory $authorEntityFactory
     */
    private $authorEntityFactory;

    /**
     * AuthorDataMapper constructor.
     * @param BookEntityFactory $bookEntityFactory
     * @param AuthorEntityFactory $authorEntityFactory
     * @param HeadingEntityFactory $headingEntityFactory
     */
    public function __construct(
        BookEntityFactory $bookEntityFactory,
        AuthorEntityFactory $authorEntityFactory,
        HeadingEntityFactory $headingEntityFactory
    ) {
        parent::__construct($bookEntityFactory, $headingEntityFactory, $authorEntityFactory);
        $this->authorEntityFactory = $authorEntityFactory;
    }

    /**
     * @param Author $data
     * @param array $books
     * @return AuthorEntity
     */
    public function setData(Author $data, array $books): AuthorEntity
    {
        return $this->authorEntityFactory->create()->setId($data->getId())
            ->setName($data->getName())
            ->setSurname($data->getSurname())
            ->setBooks($books);
    }

    /**
     * @param AuthorEntity $entity
     * @return array
     */
    public function toArray(AuthorEntity $entity): array
    {
        return [
            'id'        => $entity->getId(),
            'name'      => $entity->getName(),
            'surname'   => $entity->getSurname(),
            'books'     => $entity->getBooks()
        ];
    }
}
