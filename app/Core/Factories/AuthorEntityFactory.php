<?php

namespace App\Core\Factories;

use App\Core\Entities\AuthorEntity;

/**
 * Class AuthorEntityFactory
 * @package App\Core\Factories
 */
class AuthorEntityFactory
{
    /**
     * @return AuthorEntity
     */
    public function create(): AuthorEntity
    {
        return new AuthorEntity();
    }
}
