<?php

namespace App\Core\Factories;

use App\Core\Entities\BookEntity;

/**
 * Class BookEntityFactory
 * @package App\Core\Factories
 */
class BookEntityFactory
{
    /**
     * @return BookEntity
     */
    public function create(): BookEntity
    {
        return new BookEntity();
    }
}
