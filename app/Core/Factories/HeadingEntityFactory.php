<?php

namespace App\Core\Factories;

use App\Core\Entities\HeadingEntity;

/**
 * Class HeadingEntityFactory
 * @package App\Core\Factories
 */
class HeadingEntityFactory
{
    /**
     * @return HeadingEntity
     */
    public function create(): HeadingEntity
    {
        return new HeadingEntity();
    }
}
