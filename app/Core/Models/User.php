<?php

namespace App\Core\Models;

use App\Core\Services\UserService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Core\Models
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var UserService $userService
     */
    private $userService;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * @var array $hidden
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection
     */
    public function getRoles(): ?Collection
    {
        return $this->roles;
    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string $permissionName
     * @return bool
     */
    public function checkPermission(string $permissionName): bool
    {
        foreach ($this->getRoles() as $role) {
            foreach ($role->getPermissions() as $permission) {

                /** @var Permission $permission */
                if (str_is($permissionName, $permission->getName())) {
                    return true;
                }
            }
        }

        return false;
    }
}
