<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Heading
 * @package App\Core\Models
 */
class Heading extends Model
{
    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection|null
     */
    public function getBooks(): ?Collection
    {
        return $this->books;
    }

    /**
     * @return BelongsToMany
     */
    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class)->orderBy('id', 'DESC');
    }
}
