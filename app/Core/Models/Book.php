<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Book
 * @package App\Core\Models
 */
class Book extends Model
{
    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'photo',
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhotoName(): ?string
    {
        return $this->photo;
    }

    /**
     * @return Collection|null
     */
    public function getAuthors(): ?Collection
    {
        return $this->authors;
    }

    /**
     * @return Collection|null
     */
    public function getHeadings(): ?Collection
    {
        return $this->headings;
    }

    /**
     * @return BelongsToMany
     */
    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class)->orderBy('id', 'DESC');
    }

    /**
     * @return BelongsToMany
     */
    public function headings(): BelongsToMany
    {
        return $this->belongsToMany(Heading::class)->orderBy('id', 'DESC');
    }
}
