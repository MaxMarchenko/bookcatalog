<?php

namespace App\Core\Entities;

/**
 * Class AuthorEntity
 * @package App\Core\Entities
 */
class AuthorEntity implements EntityInterface
{
    private const FULL_NAME_PATTERN = '%s %s';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $surname
     */
    private $surname;

    /**
     * @var array $books
     */
    private $books;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AuthorEntity
     */
    public function setId(int $id): AuthorEntity
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AuthorEntity
     */
    public function setName(string $name): AuthorEntity
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return AuthorEntity
     */
    public function setSurname(string $surname): AuthorEntity
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return array
     */
    public function getBooks(): array
    {
        return $this->books;
    }

    /**
     * @param array $books
     * @return AuthorEntity
     */
    public function setBooks(array $books): AuthorEntity
    {
        $this->books = $books;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return sprintf(self::FULL_NAME_PATTERN, $this->getName(), $this->getSurname());
    }
}
