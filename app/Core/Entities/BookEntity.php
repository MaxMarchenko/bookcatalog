<?php

namespace App\Core\Entities;

/**
 * Class BookEntity
 * @package App\Core\Entities
 */
class BookEntity implements EntityInterface
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $photoPath
     */
    private $photoPath;

    /**
     * @var array $authors
     */
    private $authors;

    /**
     * @var array $headings
     */
    private $headings;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BookEntity
     */
    public function setId(int $id): BookEntity
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BookEntity
     */
    public function setName(string $name): BookEntity
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhotoPath(): ?string
    {
        return $this->photoPath;
    }

    /**
     * @param string $photoPath
     * @return BookEntity
     */
    public function setPhotoPath(?string $photoPath): BookEntity
    {
        $this->photoPath = $photoPath;

        return $this;
    }

    /**
     * @return array
     */
    public function getAuthors(): array
    {
        return $this->authors;
    }

    /**
     * @param array $authors
     * @return BookEntity
     */
    public function setAuthors(array $authors): BookEntity
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getHeadings(): ?array
    {
        return $this->headings;
    }

    /**
     * @param array $headings
     * @return BookEntity
     */
    public function setHeadings(array $headings): BookEntity
    {
        $this->headings = $headings;

        return $this;
    }

    /**
     * @return string
     */
    public function getStringAuthors(): string
    {
        $authors = [];
        foreach ($this->getAuthors() as $author) {
            $authors[] = $author->getFullName();
        }

        return implode(',', $authors);
    }

    public function getStringHeadings(): string
    {
        $headings = [];
        foreach ($this->getHeadings() as $heading) {
            $headings[] = $heading->getName();
        }

        return implode(',', $headings);
    }
}
