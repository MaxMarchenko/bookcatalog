<?php

namespace App\Core\Entities;

/**
 * Class HeadingEntityFactory
 * @package App\Core\Entities
 */
class HeadingEntity implements EntityInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var array $books
     */
    private $books;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return HeadingEntity
     */
    public function setId(int $id): HeadingEntity
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return HeadingEntity
     */
    public function setName(string $name): HeadingEntity
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getBooks(): array
    {
        return $this->books;
    }

    /**
     * @param array $books
     * @return HeadingEntity
     */
    public function setBooks(array $books): HeadingEntity
    {
        $this->books = $books;

        return $this;
    }
}
