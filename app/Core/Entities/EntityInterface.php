<?php

namespace App\Core\Entities;

/**
 * Interface EntityInterface
 * @package App\Core\Entities
 */
interface EntityInterface
{
    /**
     * @return int
     */
    public function getId(): int;
}
