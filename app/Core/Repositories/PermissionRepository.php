<?php

namespace App\Core\Repositories;

use App\Core\Models\Permission;

/**
 * Class PermissionRepository
 * @package App\Core\Repositories
 */
class PermissionRepository extends AbstractDefaultRepository
{
    /**
     * @var Permission $model
     */
    protected $model;

    /**
     * PermissionRepository constructor.
     * @param Permission $model
     */
    public function __construct(Permission $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
