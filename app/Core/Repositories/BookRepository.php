<?php

namespace App\Core\Repositories;

use App\Core\Models\Book;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BookRepository
 * @package App\Core\Repositories
 */
class BookRepository extends AbstractDefaultRepository
{
    /**
     * @var Book $model
     */
    protected $model;

    /**
     * BookRepository constructor.
     * @param Book $model
     */
    public function __construct(Book $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param array|null $authors
     * @param Model $model
     * @return array
     */
    public function addAuthors(?array $authors, Model $model): ?array
    {
        /** @var Book $model */
        return $model->authors()->sync($authors);
    }

    /**
     * @param array|null $headings
     * @param Model $model
     * @return array
     */
    public function addHeadings(?array $headings, Model $model): ?array
    {
        /** @var Book $model */
        return $model->headings()->sync($headings);
    }
}
