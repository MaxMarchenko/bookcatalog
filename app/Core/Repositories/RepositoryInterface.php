<?php

namespace App\Core\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Core\Repositories
 */
interface RepositoryInterface
{
    /**
     * @param int $id
     * @return Model|null
     */
    public function getOne(int $id): ?Model;

    /**
     * @param array $where
     * @param array $select
     * @return Model|null
     */
    public function getOneWithClause(array $where, array $select): ?Model;

    /**
     * @return Collection
     */
    public function getAll(): ?Collection;

    /**
     * @param array $where
     * @param array $select
     * @return Collection
     */
    public function getAllWithClause(array $where, array $select): ?Collection;

    /**
     * @param array $fields
     * @return Model
     */
    public function save(array $fields): Model;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
