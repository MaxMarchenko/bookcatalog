<?php

namespace App\Core\Repositories;

use App\Core\Models\User;

class UserRepository extends AbstractDefaultRepository
{
    /**
     * @var User $model;
     */
    protected $model;

    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
