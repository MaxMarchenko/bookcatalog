<?php

namespace App\Core\Repositories;

use App\Core\Models\Author;

/**
 * Class AuthorRepository
 * @package App\Core\Repositories
 */
class AuthorRepository extends AbstractDefaultRepository
{

    /**
     * @var Author $model
     */
    protected $model;

    /**
     * AuthorRepository constructor.
     * @param Author $model
     */
    public function __construct(Author $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
