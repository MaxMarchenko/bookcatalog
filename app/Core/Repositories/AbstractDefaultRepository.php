<?php

namespace App\Core\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractDefaultRepository implements RepositoryInterface
{
    public const NAME_COLUMN = 'name';

    /**
     * @var Model $model
     */
    protected $model;

    /**
     * AbstractDefaultRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function getOne(int $id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * @param array $where
     * @param array $select
     * @return Model|null
     */
    public function getOneWithClause(array $where = [], array $select = ['*']): ?Model
    {
        return $this->model->select($select)->where($where)->first();
    }

    /**
     * @return Collection
     */
    public function getAll(): ?Collection
    {
        return $this->model::orderBy('id', 'DESC')->get();
    }

    /**
     * @param array $where
     * @param array $select
     * @return Collection
     */
    public function getAllWithClause(array $where = [], array $select = ['*']): ?Collection
    {
        return $this->model->select($select)->where($where)->get();
    }

    /**
     * @param array $fields
     * @return Model
     */
    public function save(array $fields): Model
    {
        $this->model->fill($fields);
        $this->model->save();

        return $this->model;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model::destroy($id);
    }
}
