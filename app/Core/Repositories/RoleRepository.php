<?php

namespace App\Core\Repositories;

use App\Core\Models\Role;

/**
 * Class RoleRepository
 * @package App\Core\Repositories
 */
class RoleRepository extends AbstractDefaultRepository
{
    /**
     * @var Role $model
     */
    protected $model;

    /**
     * RoleRepository constructor.
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
