<?php

namespace App\Core\Repositories;

use App\Core\Models\Heading;

/**
 * Class HeadingRepository
 * @package App\Core\Repositories
 */
class HeadingRepository extends AbstractDefaultRepository
{
    /**
     * @var Heading $model
     */
    protected $model;

    /**
     * HeadingRepository constructor.
     * @param Heading $model
     */
    public function __construct(Heading $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
}
