<?php

namespace App\Core\Services;

use App\Core\DataMappers\BasicDataMapper;
use App\Core\DataMappers\HeadingDataMapper;
use App\Core\Entities\HeadingEntity;
use App\Core\Models\Heading;
use App\Core\Repositories\HeadingRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class HeadingService
 * @package App\Core\Services
 */
class HeadingService extends AbstractService
{
    /**
     * @var HeadingRepository $headingRepository
     */
    protected $headingRepository;

    /**
     * @var HeadingDataMapper $headingDataMapper
     */
    protected $headingDataMapper;

    /**
     * BooksService constructor.
     * @param BasicDataMapper $basicDataMapper
     * @param HeadingRepository $headingRepository
     * @param HeadingDataMapper $headingDataMapper
     */
    public function __construct(
        BasicDataMapper $basicDataMapper,
        HeadingRepository $headingRepository,
        HeadingDataMapper $headingDataMapper
    ) {
        parent::__construct($basicDataMapper);
        $this->headingRepository = $headingRepository;
        $this->headingDataMapper = $headingDataMapper;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->getSetData($this->headingRepository->getAll());
    }

    /**
     * @param array $where
     * @return HeadingEntity|null
     */
    public function getOneWithClause(array $where): ?HeadingEntity
    {
        $heading = $this->headingRepository->getOneWithClause($where);
        if ($heading !== null) {
            /** @var Heading $heading **/
            $books = $this->getBooks($heading->getBooks());

            return $this->headingDataMapper->setData($heading, $books);
        }

        return null;
    }

    /**
     * @param Collection $collectionHeadings
     * @return array
     */
    private function getSetData(Collection $collectionHeadings): array
    {
        $headingEntitiesArray = [];

        foreach ($collectionHeadings as $heading) {
            $books = $this->getBooks($heading->getBooks());

            $headingEntitiesArray[] = $this->headingDataMapper->setData($heading, $books);
        }

        return $headingEntitiesArray;
    }
}
