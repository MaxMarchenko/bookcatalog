<?php

namespace App\Core\Services;

use App\Core\DataMappers\BasicDataMapper;
use App\Core\DataMappers\BookDataMapper;
use App\Core\Entities\HeadingEntity;
use App\Core\Models\Book;
use App\Core\Repositories\BookRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Core\Entities\AuthorEntity;

/**
 * Class BooksService
 * @package App\Core\Services
 */
class BooksService extends AbstractService
{
    /**
     * @var BookRepository $bookRepository
     */
    protected $bookRepository;

    /**
     * @var BookDataMapper $bookDataMapper
     */
    protected $bookDataMapper;

    /**
     * @var AuthorService $authorService
     */
    protected $authorService;

    /**
     * @var HeadingService $headingService
     */
    protected $headingService;

    /**
     * BooksService constructor.
     * @param BasicDataMapper $basicDataMapper
     * @param BookRepository $bookRepository
     * @param BookDataMapper $bookDataMapper
     * @param AuthorService $authorService
     * @param HeadingService $headingService
     */
    public function __construct(
        BasicDataMapper $basicDataMapper,
        BookRepository $bookRepository,
        BookDataMapper $bookDataMapper,
        AuthorService $authorService,
        HeadingService $headingService
    ) {
        parent::__construct($basicDataMapper);
        $this->bookRepository = $bookRepository;
        $this->bookDataMapper = $bookDataMapper;
        $this->authorService = $authorService;
        $this->headingService = $headingService;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->getSetData($this->bookRepository->getAll());
    }

    /**
     * @param Collection $collectionBooks
     * @return array
     */
    private function getSetData(Collection $collectionBooks): array
    {
        $bookEntitiesArray = [];

        foreach ($collectionBooks as $book) {
            $authors = $this->getAuthors($book->getAuthors());
            $headings = $this->getHeadings($book->getHeadings());

            $bookEntitiesArray[] = $this->bookDataMapper->setData($book, $authors, $headings);
        }

        return $bookEntitiesArray;
    }

    /**
     * @return array
     */
    public function getAuthorNamesArray(): array
    {
        $authors = [];
        foreach ($this->authorService->getAll() as $value) {

            /** @var AuthorEntity $value **/
            $authors[$value->getId()] = $value->getFullName();
        }

        return $authors;
    }

    /**
     * @return array
     */
    public function getHeadingNamesArray(): array
    {
        $headings = [];

        foreach ($this->headingService->getAll() as $value) {

            /** @var HeadingEntity $value */
            $headings[$value->getId()] = $value->getName();
        }

        return $headings;
    }

    /**
     * @param int $bookId
     * @return \Illuminate\Support\Collection|null
     */
    public function getAuthorsId(int $bookId): ?\Illuminate\Support\Collection
    {
        $book = $this->bookRepository->getOne($bookId);

        /** @var Book $book */
        if (($book !== null) && $book->getAuthors() !== null) {
            return $book->getAuthors()->pluck('id');
        }

        return null;
    }

    /**
     * @param int $bookId
     * @return \Illuminate\Support\Collection|null
     */
    public function getHeadingsId(int $bookId): ?\Illuminate\Support\Collection
    {
        $book = $this->bookRepository->getOne($bookId);

        /** @var Book $book */
        if (($book !== null) && $book->getHeadings() !== null) {
            return $book->getHeadings()->pluck('id');
        }

        return null;
    }
}
