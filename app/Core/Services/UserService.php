<?php

namespace App\Core\Services;

use App\Core\Repositories\UserRepository;

/**
 * Class UserService
 * @package App\Core\Services
 */
class UserService
{
    /**
     * @var UserRepository $userRepository
     */
    protected $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    protected function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
}
