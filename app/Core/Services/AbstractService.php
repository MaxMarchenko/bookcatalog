<?php

namespace App\Core\Services;

use App\Core\DataMappers\BasicDataMapper;
use App\Core\Models\Book;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AbstractService
 * @package App\Core\Services
 */
abstract class AbstractService
{
    /**
     * @var BasicDataMapper $basicDataMapper
     */
    protected $basicDataMapper;

    /**
     * AbstractService constructor.
     * @param BasicDataMapper $basicDataMapper
     */
    public function __construct(BasicDataMapper $basicDataMapper)
    {
        $this->basicDataMapper = $basicDataMapper;
    }

    /**
     * @param Collection $collectionBooks
     * @return array
     */
    protected function getBooks(Collection $collectionBooks): array
    {
        $booksEntitiesArray = [];

        foreach ($collectionBooks as $book) {
            /** @var Book $book */
            $booksEntitiesArray[] = $this->basicDataMapper->setBooksData(
                $book,
                $this->getAuthors($book->getAuthors()),
                $this->getHeadings($book->getHeadings())
            );
        }

        return $booksEntitiesArray;
    }

    /**
     * @param Collection $collectionAuthors
     * @return array
     */
    protected function getAuthors(Collection $collectionAuthors): ?array
    {
        $authorEntitiesArray = [];

        foreach ($collectionAuthors as $author) {
            $authorEntitiesArray[] = $this->basicDataMapper->setAuthorsData($author);
        }

        return $authorEntitiesArray;
    }

    /**
     * @param Collection $collectionHeadings
     * @return array
     */
    protected function getHeadings(Collection $collectionHeadings): array
    {
        $headingEntitiesArray = [];

        foreach ($collectionHeadings as $heading) {
            $headingEntitiesArray[] = $this->basicDataMapper->setHeadingsData($heading);
        }

        return $headingEntitiesArray;
    }
}
