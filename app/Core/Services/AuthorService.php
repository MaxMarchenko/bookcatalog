<?php

namespace App\Core\Services;

use App\Core\DataMappers\BasicDataMapper;
use App\Core\DataMappers\AuthorDataMapper;
use App\Core\Entities\AuthorEntity;
use App\Core\Models\Author;
use App\Core\Repositories\AuthorRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AuthorService
 * @package App\Core\Services
 */
class AuthorService extends AbstractService
{
    /**
     * @var AuthorRepository $authorRepository
     */
    protected $authorRepository;

    /**
     * @var AuthorDataMapper $authorDataMapper
     */
    protected $authorDataMapper;

    /**
     * BooksService constructor.
     * @param BasicDataMapper $basicDataMapper
     * @param AuthorRepository $authorRepository
     * @param AuthorDataMapper $authorDataMapper
     */
    public function __construct(
        BasicDataMapper $basicDataMapper,
        AuthorRepository $authorRepository,
        AuthorDataMapper $authorDataMapper
    ) {
        parent::__construct($basicDataMapper);
        $this->authorRepository = $authorRepository;
        $this->authorDataMapper = $authorDataMapper;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->getSetData($this->authorRepository->getAll());
    }

    /**
     * @param array $where
     * @return AuthorEntity|null
     */
    public function getOneWithClause(array $where): ?AuthorEntity
    {
        $author = $this->authorRepository->getOneWithClause($where);
        if ($author !== null) {
            /** @var Author $author **/
            $books = $this->getBooks($author->getBooks());

            return $this->authorDataMapper->setData($author, $books);
        }

        return null;
    }

    /**
     * @param Collection $collectionAuthors
     * @return array
     */
    private function getSetData(Collection $collectionAuthors): array
    {
        $authorEntitiesArray = [];

        foreach ($collectionAuthors as $author) {
            $books = $this->getBooks($author->getBooks());

            $authorEntitiesArray[] = $this->authorDataMapper->setData($author, $books);
        }

        return $authorEntitiesArray;
    }
}
