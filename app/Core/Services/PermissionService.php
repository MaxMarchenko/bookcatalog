<?php

namespace App\Core\Services;

use App\Core\Repositories\PermissionRepository;
use App\Core\Repositories\RoleRepository;

class PermissionService
{
    /**
     * @var RoleRepository $roleRepository
     */
    protected $roleRepository;

    /**
     * @var PermissionRepository $permissionRepository
     */
    protected $permissionRepository;

    /**
     * PermissionService constructor.
     * @param RoleRepository $roleRepository
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }
}
