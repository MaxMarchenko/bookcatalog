<?php

namespace App\Core\Services;

use App\Core\Repositories\RoleRepository;

class RoleService
{
    /**
     * @var RoleRepository $roleRepository
     */
    protected $roleRepository;

    /**
     * RoleService constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }
}
