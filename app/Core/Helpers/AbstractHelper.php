<?php

namespace App\Core\Helpers;

/**
 * @property AbstractHelper instance
 */
class AbstractHelper
{
    /**
     * @var static $instance
     */
    protected static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return static::$instance;
    }
}
