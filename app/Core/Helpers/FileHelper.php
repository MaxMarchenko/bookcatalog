<?php

namespace App\Core\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileHelper
 * @package App\Core\Helpers
 */
class FileHelper extends AbstractHelper
{
    public const FILE_FOLDER_IMAGES = 'public/images';
    public const DELETE_IMAGE_PATH = 'public/images/%s';
    private const FILE_NAME_PATTERN = '%s.%s';

    /**
     * @param UploadedFile $file
     * @param string $folder
     * @return null|string
     */
    public function upload(UploadedFile $file, string $folder): ?string
    {
        if ($file !== null) {
            $filename = sprintf(self::FILE_NAME_PATTERN, str_random(10), $file->extension());
            $file->storeAs($folder, $filename);

            return $filename;
        }

        return null;
    }

    /**
     * @param string $fileName
     * @param string $filePath
     * @return bool
     */
    public function drop(string $fileName, string $filePath): bool
    {
        return Storage::delete(sprintf($filePath, $fileName));
    }
}
