<?php

namespace App\Core\Helpers;

/**
 * Class DebugHelper
 * @package App\Core\Helpers
 */
class DebugHelper extends AbstractHelper
{
    /**
     * @param mixed $subject
     * @return void
     */
    public function customDebug($subject): void
    {
        echo '<pre>';
        print_r($subject);
        echo '</pre>';
        die;
    }
}
