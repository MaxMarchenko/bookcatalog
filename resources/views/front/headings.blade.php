@extends('front.layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">Headings Page</h1>

        <div class="contents row">
            @foreach($headings as $heading)
                <div class="col-lg-4 col-sm-6 portfolio-item">
                    <div class="card h-100">
                        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{route('headingBooks', $heading->getName())}}">{{$heading->getName()}}</a>;
                            </h4>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="wrapper">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <ul id="pagination" class="pagination-sm"></ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container -->
@endsection

