@extends('front.layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">Books By
            <small>{{$author->getFullName()}}</small>
        </h1>

        <div class="wrapper">
            <nav id="sidebar">
                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Authors</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            @foreach($authors as $authorValue)
                                <li>
                                    <a href="{{route('authorBooks', $authorValue->getName())}}">{{$authorValue->getFullName()}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="contents">
            @foreach($author->getBooks() as $book)
                <div class="row">
                    <div class="col-md-7">
                        <a href="#">
                            <img class="img-fluid rounded mb-3 mb-md-0" src="{{$book->getPhotoPath()}}" alt="">
                        </a>
                    </div>
                    <div class="col-md-5">
                        <h3>{{$book->getName()}}</h3>
                        <h5>
                            Authors:
                            @foreach($book->getAuthors() as $author)
                                <a href="{{route('authorBooks', $author->getName())}}">{{$author->getFullName()}}</a>;
                            @endforeach
                        </h5>
                        <h5>
                            Headings:
                            @foreach($book->getHeadings() as $heading)
                                <a href="{{route('headingBooks', $heading->getName())}}">{{$heading->getName()}}</a>;
                            @endforeach
                        </h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>


        <div class="wrapper">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <ul id="pagination" class="pagination-sm"></ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container -->
@endsection

