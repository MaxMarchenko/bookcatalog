<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Book Catalog') }}</title>

    <link href="{{ asset('toolkits/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('front-page') }}">{{ config('app.name', 'Book Catalog') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('front-page') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('authors-page') }}">Authors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('headings-page') }}">Headings</a>
                </li>
                @if (Route::has('login'))
                        @auth
                            @if(Auth::user()->checkPermission(App\Core\Models\Permission::VIEW_ADMIN))
                            <li class="nav-item">
                                <a href="{{ url('/admin') }}" class="nav-link">Admin</a>
                            </li>
                            @endif
                        @else
                        <li class="nav-item">
                            <a href="{{ route('login') }}" class="nav-link">Login</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('register') }}" class="nav-link">Register</a>
                        </li>
                        @endauth
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('toolkits/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('toolkits/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('toolkits/pagination.js') }}"></script>
<script>
    $(document).ready(function()
    {
        $("#tab").pagination({
            items: 10,
            contents: 'contents',
            previous: 'Previous',
            next: 'Next',
            position: 'bottom',
        });
    });
</script>

</body>

</html>
