@extends('admin.layouts.app')

@section('content')
    <div class="container">
        @include('admin.book.modal.add')
        @include('admin.book.modal.edit')
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Books</div>
                    <div class="card-body">
                        <div class="create create-js">
                            <button class="btn btn-success add-modal-js"><i class="fa fa-plus"></i></button>
                        </div>
                        <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Photo</th>
                                <th>Authors</th>
                                <th>Headings</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $book)
                                <tr>
                                    <td id="name">{{$book->getName()}}</td>
                                    <td> <img src="{{$book->getPhotoPath()}}" class="img-responsive" id="preview-image" width="100%"></td>
                                    <td>{{$book->getStringAuthors()}}</td>
                                    <td>{{$book->getStringHeadings()}}</td>
                                    <td class="actions">
                                        <button class="btn btn-primary edit-js" data-id="{{$book->getId()}}"><i class="fa fa-edit"></i></button>
                                        <button class="delete-js btn btn-danger" data-id="{{$book->getId()}}" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-remove"></i>
                                        </button>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Photo</th>
                                <th>Authors</th>
                                <th>Headings</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
