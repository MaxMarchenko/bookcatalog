<div id="add-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" enctype="multipart/form-data" id="upload-file-js">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="image-add-js">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="" name="image" id="image-add-js" multiple>
                            <img src="" class="img-responsive" id="preview-image" width="100%">
                            <p class="error-photo text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name-add-js">Name*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name-add-js" autofocus>
                            <br>
                            <p class="error-name text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Authors*</label>
                        <div class="col-sm-10" id="select2">
                            {{Form::select('authors[]',$authors, null, [
                               'class' => 'form-control',
                                'id' => 'authors-add',
                                'multiple'=>'multiple','data-placeholder'=>'Select authors'])
                            }}
                            <br>
                            <p class="error-authors text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Headings*</label>
                        <div class="col-sm-10" id="select2">
                            {{Form::select('headings[]',$headings, null, [
                               'class' => 'form-control',
                                'id' => 'headings-add',
                                'multiple'=>'multiple','data-placeholder'=>'Select headings'])
                            }}
                            <br>
                            <p class="error-headings text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success add-js" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Add
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
