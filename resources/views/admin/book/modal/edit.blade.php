<div id="edit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" enctype="multipart/form-data" id="upload-file-edit-js">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="edit-id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-id" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="image-add-js">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="" name="image" id="image-edit-js" multiple>
                            <img src="" class="img-responsive" id="preview-image-edit" width="100%">
                            <br>
                            <p class="error-photo text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="edit-name">Book name*</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-name" autofocus>
                            <p class="error-name text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Authors*</label>
                        <div class="col-sm-10">
                            {{Form::select('authors[]', $authors, null, [
                               'class' => 'select2 form-control',
                                'id' => 'authors-edit',
                                'multiple'=>'multiple','data-placeholder'=>'Select authors'])
                            }}
                            <br>
                            <p class="error-authors text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Headings*</label>
                        <div class="col-sm-10">
                            {{Form::select('headings[]', $headings, null, [
                               'class' => 'select2 form-control',
                                'id' => 'headings-edit',
                                'multiple'=>'multiple','data-placeholder'=>'Select headings'])
                            }}
                            <br>
                            <p class="error-headings text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material edit" data-dismiss="modal">
                        <span class='glyphicon glyphicon-check'></span> Edit
                    </button>
                    <button type="button" class="btn btn-material" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
