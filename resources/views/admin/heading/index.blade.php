@extends('admin.layouts.app')

@section('content')
    <div class="container">
        @include('admin.heading.modal.add')
        @include('admin.heading.modal.edit')
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Headings</div>
                    <div class="card-body">
                        <div class="create create-js">
                            <button class="btn btn-success add-heading-js"><i class="fa fa-plus"></i></button>
                        </div>
                        <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($headings as $heading)
                                <tr>
                                    <td id="name">{{$heading->getName()}}</td>
                                    <td class="actions">
                                        <button class="btn btn-primary edit-heading-js" data-id="{{$heading->getId()}}"><i class="fa fa-edit"></i></button>
                                        <button class="delete-heading-js btn btn-danger" data-id="{{$heading->getId()}}" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
