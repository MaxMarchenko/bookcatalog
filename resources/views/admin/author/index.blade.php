@extends('admin.layouts.app')

@section('content')
    <div class="container">
        @include('admin.author.modal.add')
        @include('admin.author.modal.edit')
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Authors</div>
                    <div class="card-body">
                        <div class="create create-js">
                            <button class="btn btn-success add-author-js"><i class="fa fa-plus"></i></button>
                        </div>
                        <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($authors as $author)
                                <tr>
                                    <td id="name">{{$author->getName()}}</td>
                                    <td id="surname">{{$author->getSurname()}}</td>
                                    <td class="actions">
                                        <button class="btn btn-primary edit-author-js" data-id="{{$author->getId()}}"><i class="fa fa-edit"></i></button>
                                        <button class="delete-author-js btn btn-danger" data-id="{{$author->getId()}}" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
