<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <nav id="sidebar">
                <ul class="list-unstyled components">
                    <li>
                        <a href="{{url('/admin')}}">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/book')}}">Books</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/author')}}">Authors</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/heading')}}">Headings</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

