$(function() {
    $('#image-add-js').change(function(){
        let file=this.files[0];
        let imageType = file.type;
        let extentions = ["image/jpeg","image/png","image/jpg","image/gif", "image/svg+xml"];

        if (extentions.indexOf(imageType) >= 0 ){
            toastr.success('File successfully download!', {timeOut: 5000});
            FileUploadAjax();
        } else {
            toastr.error('File Is Invalid!', {timeOut: 5000});
            return false;
        }
    });
});
function FileUploadAjax(){
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': token
        },
        url: 'book/upload-image',
        type:'POST',
        data: new FormData($('#upload-file-js').get(0)),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
            $('#image-add-js').hide();
            $('#preview-image').attr('src',"/storage/images/"+data);
        }, error:function (error) {
            console.log(error);
        }
    });
}

$(function() {
    $('#image-edit-js').change(function(){
        let file=this.files[0];
        let imageType = file.type;
        let extentions = ["image/jpeg","image/png","image/jpg","image/gif", "image/svg+xml"];

        if(extentions.indexOf(imageType) >= 0 ){
            toastr.success('File successfully download!', {timeOut: 5000});
            FileUploadAjaxEdit();
        }else {
            toastr.error('File Is Invalid!', {timeOut: 5000});
            $('#previewImageEdit').attr('src','/images/no-image.png');
            return false;
        }
    });
});
function FileUploadAjaxEdit(){
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': token
        },
        url: 'book/upload-image',
        type:'POST',
        data: new FormData($('#upload-file-edit-js').get(0)),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
            $('#image-edit-js').hide();
            $('#preview-image-edit').attr('src',"/storage/images/"+data);
        },
        error:function (error) {
            console.log(error);
        }
    });
}
