$(document).on('click', '.add-modal-js', function() {
    $('#authors-add').select2();
    $('#headings-add').select2();
    $('#add-modal').modal('show');
});

let modalFooter = $('.modal-footer');
let errorNameClass = $('.error-name');
let errorSurnameClass = $('.error-surname');
let errorAuthorsClass = $('.error-authors');
let errorHeadingsClass = $('.error-headings');
let errorPhotoClass = $('.error-photo');
let token = $('input[name=_token]').val();


modalFooter.on('click', '.add-js', function() {
    $.ajax({
        type: 'POST',
        url: 'book',
        data: {
            '_token': $('input[name=_token]').val(),
            'name':  $('#name-add-js').val(),
            'photo': $('#preview-image').attr('src'),
            'authors': $('#authors-add').val(),
            'headings': $('#headings-add').val()
        },
        success: function() {
            toastr.success('Successfully add Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#add-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.photo) {
                    errorPhotoClass.removeClass('hidden');
                    errorPhotoClass.text(error.responseJSON.errors.photo);
                }

                if (error.responseJSON.errors.name) {
                    errorNameClass.removeClass('hidden');
                    errorNameClass.text(error.responseJSON.errors.name);
                }
                if (error.responseJSON.errors.authors) {
                    errorAuthorsClass.removeClass('hidden');
                    errorAuthorsClass.text(error.responseJSON.errors.authors);
                }
                if (error.responseJSON.errors.headings) {
                    errorHeadingsClass.removeClass('hidden');
                    errorHeadingsClass.text(error.responseJSON.errors.headings);
                }
            }
        },
    });
});

$(document).on('click', '.edit-js', function() {
    let id = $(this).data("id");

    $('#edit-id').val(id);
    $('#edit-name').val($(this).parent().parent().children("td#name").text());

    let headingsEdit = $('#headings-edit');
    let authorsEdit = $('#authors-edit');

    headingsEdit.select2();
    authorsEdit.select2();

    getAuthorsId(id);
    getHeadingsId(id);
    $('#edit-modal').modal('show');
});

modalFooter.on('click', '.edit', function() {

    let id = $('#edit-id').val();

    $.ajax({
        type: 'PUT',
        url: 'book/' + id,
        data: {
            '_token': token,
            'id': id,
            'name': $('#edit-name').val(),
            'photo': $('#preview-image-edit').attr('src'),
            'authors': $('#authors-edit').val(),
            'headings': $('#headings-edit').val()
        },
        success: function() {
            toastr.success('Successfully edit Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#edit-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.photo) {
                    errorPhotoClass.removeClass('hidden');
                    errorPhotoClass.text(error.responseJSON.errors.photo);
                }

                if (error.responseJSON.errors.name) {
                    errorNameClass.removeClass('hidden');
                    errorNameClass.text(error.responseJSON.errors.name);
                }
                if (error.responseJSON.errors.authors) {
                    errorAuthorsClass.removeClass('hidden');
                    errorAuthorsClass.text(error.responseJSON.errors.authors);
                }
                if (error.responseJSON.errors.headings) {
                    errorHeadingsClass.removeClass('hidden');
                    errorHeadingsClass.text(error.responseJSON.errors.headings);
                }
            }
        },
    });
});

$(".delete-js").click(function(){
    let id = $(this).data("id");
    $.ajax(
        {
            url:  "book/" + id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                '_token': token,
                "id": id,
                "_method": 'DELETE',
            },
            success: function () {
                toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 1000});
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            error: function (error) {
                console.log(error);
            },
        });
});

function getAuthorsId(id) {
    $.ajax({
        type: 'POST',
        url: 'book/authors/id',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id
        },
        success: function(data) {
            console.log(data);
            $('#authors-edit').val(data).trigger('change');
        },
        error: function (error) {
            console.log(error);
        },
    });
}

function getHeadingsId(id) {
    $.ajax({
        type: 'POST',
        url: 'book/headings/id',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id
        },
        success: function(data) {
            $('#headings-edit').val(data).trigger('change');
        },
        error: function (error) {
            console.log(error);
        },
    });
}

$(document).on('click', '.add-author-js', function() {
    $('#add-modal').modal('show');
});
modalFooter.on('click', '.add-author', function() {
    $.ajax({
        type: 'POST',
        url: 'author',
        data: {
            '_token': $('input[name=_token]').val(),
            'name':  $('#name-add-js').val(),
            'surname': $('#surname-add-js').val(),
        },
        success: function() {
            toastr.success('Successfully add Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#add-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

               }
                if (error.responseJSON.errors.name) {
                    errorNameClass.removeClass('hidden');
                    errorNameClass.text(error.responseJSON.errors.name);
                }
                if (error.responseJSON.errors.surname) {
                    errorSurnameClass.removeClass('hidden');
                    errorSurnameClass.text(error.responseJSON.errors.surname);
                }
        },
    });
});

$(document).on('click', '.edit-author-js', function() {
    let id = $(this).data("id");

    $('#edit-id').val(id);
    $('#edit-name').val($(this).parent().parent().children("td#name").text());
    $('#edit-surname').val($(this).parent().parent().children("td#surname").text());

    $('#edit-modal').modal('show');
});

modalFooter.on('click', '.edit-author', function() {

    let id = $('#edit-id').val();

    $.ajax({
        type: 'PUT',
        url: 'author/' + id,
        data: {
            '_token': token,
            'id': id,
            'name': $('#edit-name').val(),
            'surname': $('#edit-surname').val(),
        },
        success: function() {
            toastr.success('Successfully edit Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#edit-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.name) {
                    errorNameClass.removeClass('hidden');
                    errorNameClass.text(error.responseJSON.errors.name);
                }
                if (error.responseJSON.errors.surname) {
                    errorSurnameClass.removeClass('hidden');
                    errorSurnameClass.text(error.responseJSON.errors.surname);
                }
            }
        },
    });
});

$(".delete-author-js").click(function(){
    let id = $(this).data("id");
    $.ajax(
        {
            url:  "author/" + id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                '_token': token,
                "id": id,
                "_method": 'DELETE',
            },
            success: function () {
                toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 1000});
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            error: function (error) {
                console.log(error);
            },
        });
});

$(document).on('click', '.add-heading-js', function() {
    $('#add-modal').modal('show');
});
modalFooter.on('click', '.add-heading', function() {
    $.ajax({
        type: 'POST',
        url: 'heading',
        data: {
            '_token': $('input[name=_token]').val(),
            'name':  $('#name-add-js').val(),
        },
        success: function() {
            toastr.success('Successfully add Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#add-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

            }
            if (error.responseJSON.errors.name) {
                errorNameClass.removeClass('hidden');
                errorNameClass.text(error.responseJSON.errors.name);
            }
        },
    });
});

$(document).on('click', '.edit-heading-js', function() {
    let id = $(this).data("id");

    $('#edit-id').val(id);
    $('#edit-name').val($(this).parent().parent().children("td#name").text());

    $('#edit-modal').modal('show');
});

modalFooter.on('click', '.edit-heading', function() {

    let id = $('#edit-id').val();

    $.ajax({
        type: 'PUT',
        url: 'heading/' + id,
        data: {
            '_token': token,
            'id': id,
            'name': $('#edit-name').val(),
        },
        success: function() {
            toastr.success('Successfully edit Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#edit-modal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.name) {
                    errorNameClass.removeClass('hidden');
                    errorNameClass.text(error.responseJSON.errors.name);
                }
            }
        },
    });
});

$(".delete-heading-js").click(function(){
    let id = $(this).data("id");
    $.ajax(
        {
            url:  "heading/" + id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                '_token': token,
                "id": id,
                "_method": 'DELETE',
            },
            success: function () {
                toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 1000});
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            error: function (error) {
                console.log(error);
            },
        });
});
